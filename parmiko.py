import paramiko
import time
command = "df"

host = "172.31.31.250"
username = "ubuntu"
passwd = "password"

client = paramiko.client.SSHClient()
client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
client.connect(host, username=username, password=passwd )
stdin, stdout, stderr = client.exec_command(command)
time.sleep(5)
#stdin.close()
print(stdout.read().decode())
client.close

